window.addEventListener('load', async () => {


  $("#registerItem").on("click",(event)=>{
    event.preventDefault();
    var obj={
      id:$('#id').val(),
      name:$('#name').val(),
      location:$('#location').val(),
      quantity:$('#quantity').val(),
      cost:$('#price').val(),
      imgsrc:$('#img').val(),
      type:$('#type').val(),
      sold:false,
      postedBy:null,
      inCart:false
    }
    firebase.database().ref('Items/' + $('#id').val()).set(obj);
    var account;
    web3.eth.getAccounts(function(error, accounts) {
      var itemsInstance;
      if (error) {
        console.log(error);
      }

      account = accounts[0];

      App.contracts.Items.deployed().then(function(instance) {
        itemsInstance = instance;
  
        // Execute adopt as a transaction by sending account
        return itemsInstance.addItem(obj.id,obj.name,obj.location,obj.quantity,obj.cost,obj.imgsrc,obj.type,obj.sold,account, {from: account,gas: 200000});
      }).then((data)=>{
        console.log(data);
      }).catch(function(err) {
        console.log(err.message);
      });
    });

    App.contracts.Items.deployed().then(function(instance) {
      //console.log("uwgvebewbyc");
      itemsInstance = instance;
      return itemsInstance.getItemByID.call(123);
    }).then(function(items) {
      console.log(items);
    }).catch(function(err) {
      console.log(err.message);
    });
    
    //console.log($('#id').val());
    //console.log(obj);
  });

});