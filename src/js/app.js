var cart=[];

var updateDb_AddedToCart=(itemID)=>{
  var itemData;
  ref=database.ref('Items');
  ref.on('value',(data)=>{
    itemData=data.val()[itemID];
    itemData.inCart=true;
  },errData);
  firebase.database().ref('Items/' + itemID).set(itemData, function(error) {
    if (error) {
      console.log(error);
    } else {
      console.log("success");
    }
  });
};
var ref;
var target;
var errData=(data)=>{
  console.log(data.val());
};
var web3;

var renderData=(data)=>{

  var petsRow = $('#petsRow');
  petsRow.empty();
  var petTemplate = $('#petTemplate');
  for(var k in data.val()) {
    petTemplate.find('.panel-title').text(data.val()[k].name);
    petTemplate.find('img').attr('src',data.val()[k].imgsrc);
    petTemplate.find('.pet-location').text(data.val()[k].location);
    petTemplate.find('.btn-adopt').attr('id',k);
    petTemplate.find('.pet-type').text(data.val()[k].type);
    var id="#"+k;
    console.log(id);
    console.log($(id));
    console.log(data.val()[k].inCart);
    $(document).on('click',id,(event)=>{
      updateDb_AddedToCart(event.target.id);
      $('#'+event.target.id).text('addedToCart').attr('disabled', true);
    });
    petTemplate.find('.pet-quantity').text(data.val()[k].quantity);

    petsRow.append(petTemplate.html());          
  }
};

var findData=(data)=>{
  var currData;
  for(var k in data.val()) {
    if(k==target){
        currData=data.val()[k];
        itemID=k;
    }
  }
  currData.inCart=true;
  updateDb_Sold(currData,itemID);
};

App = {
  web3Provider: null,
  contracts: {},

  init: async function() {
    
    ref=database.ref('Items');
    ref.on('value',renderData,errData);
    return await App.initWeb3();
  },

  initWeb3: async function() {
    if (window.ethereum) {
      App.web3Provider = window.ethereum;
      try {
        // Request account access
        await window.ethereum.enable();
      } catch (error) {
        // User denied account access...
        console.error("User denied account access")
      }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
      App.web3Provider = window.web3.currentProvider;
    }
    // If no injected web3 instance is detected, fall back to Ganache
    else {
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
    }
    web3 = new Web3(App.web3Provider);
    

    return App.initContract();
  },

  initContract: function() {
    $.getJSON('Items.json', function(data) {
      // Get the necessary contract artifact file and instantiate it with truffle-contract
      var ItemsArtifact = data;
      App.contracts.Items = TruffleContract(ItemsArtifact);
    
      // Set the provider for our contract
      App.contracts.Items.setProvider(App.web3Provider);
    
      // Use our contract to retrieve and mark the adopted pets
      //return App.markAdopted();
    });

  },

  

};

$(window).on('load', ()=>{
  initializeFirebaseDB();
  App.init();
});
